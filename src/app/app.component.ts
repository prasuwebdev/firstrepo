import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Chart} from 'chart.js';
import {Data} from './data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FISIION LABS, Presenting the chart with Angular 6';
  data: Data[];
  url = 'http://localhost:4000/results';
  year= [];
  range = [];
  chart = [];
  arr=[];
  obj={};
  arr2=[];
  finalData=[];
  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
    this.httpClient.get(this.url).subscribe((res: Data[]) => {
      res.forEach(y => {
        for(var i in y){{
            for(var j in y[i]){
               this.arr.push(y[i][j]['data'])
            }
        }}
        });
        for(var i in this.arr){        
            this.obj={};
            this.arr2=this.arr[i].split("|");          
            this.obj={};
            this.obj['year']=this.arr2[0];
            this.obj['range']=this.arr2[1];           
            this.finalData.push(this.obj) 
         }
        this.finalData.forEach(y => {
          this.year.push(y.year);
          this.range.push(y.range);
          });
      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: this.year,
          datasets: [
            {
              data: this.range,
              borderColor: '#3cba9f',
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }
  csvContent: string;

  onFileLoad(fileLoadedEvent) {
    const textFromFileLoaded = fileLoadedEvent.target.result;              
    this.csvContent = textFromFileLoaded;     
    // alert(this.csvContent);
}
  onFileSelect(input: HTMLInputElement) {

    const files = input.files;
    var content = this.csvContent;    
    if (files && files.length) {
       
        console.log("Filename: " + files[0].name);
        console.log("Type: " + files[0].type);
        console.log("Size: " + files[0].size + " bytes");
     

        const fileToRead = files[0];

        const fileReader = new FileReader();
        fileReader.onload = this.onFileLoad;

        fileReader.readAsText(fileToRead, "UTF-8");
    }

  }
}
